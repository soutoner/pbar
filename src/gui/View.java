/*
 * Adrian Gonzalez Leiva, 2014
 * 
 * View Interface.
 */

package gui;

import java.awt.event.ActionListener;

public interface View {

	void controller(ActionListener ctr);
	void error(String m);
	void ok(String m);
	void addLogEntry(String message);
	void clear();
	
}
