package gui;

import java.awt.*;
import java.awt.event.*;
import java.sql.*;

import javax.swing.*;
import javax.swing.border.*;

import models.DBConnector;
import dataStructures.tuple.Tuple2;

/**
 * Adrian Gonzalez Leiva, 2014
 * 
 * Panel for tables.
 * 
 * @param int n (table id)
 */
public class BillPanel extends JPanel implements View{
	private static final long serialVersionUID = 1L;
	
	private JTextArea bill;
	private int id,billid;
	private double total;
	private JButton finishB, resetB, addB;
	private JTextField quantityJTF;
		
	public BillPanel(int n){
		finishB=new JButton("Finalizar Cuenta");
		resetB=new JButton("Reiniciar Cuenta");
		addB=new JButton("A�adir");
		quantityJTF=new JTextField();
		
		id=n;
		total=0;
			
		setLayout(new BorderLayout());
		
		bill=new JTextArea(30,20);
		bill.setEditable(false);
		Font font = new Font("Verdana", Font.BOLD, 14);
		bill.setFont(font);
		
		JPanel buttons =new JPanel();
		buttons.setLayout(new BorderLayout());
		
			JPanel buttonsUp=new JPanel();
			buttonsUp.setLayout(new GridLayout(3,1));
			buttonsUp.setBorder(new TitledBorder("Cuenta"));
				
				buttonsUp.add(resetB);
				buttonsUp.add(new JButton(" "));
				buttonsUp.add(finishB);
			
			buttons.add(buttonsUp,BorderLayout.NORTH);
			
			JPanel buttonsCen=new JPanel();
			buttonsCen.setLayout(new GridLayout(20,5));
			buttonsCen.setBorder(new TitledBorder("Productos"));
			
			buttons.add(buttonsCen,BorderLayout.CENTER);
			
			JPanel buttonsDown=new JPanel();
			buttonsDown.setLayout(new GridLayout(1,2));
			buttonsDown.setBorder(new TitledBorder("Cantidad"));
				
				buttonsDown.add(quantityJTF);
				buttonsDown.add(addB);
				
			buttons.add(buttonsDown,BorderLayout.SOUTH);

		add(bill,BorderLayout.WEST);
		add(buttons,BorderLayout.CENTER);
		add(new JLabel("Hola :_)"),BorderLayout.SOUTH);
		
		finishB.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent e) {
	        	DBConnector db = new DBConnector("jdbc:mysql://localhost:3306/7dobles","sout","sout00");
	    		Tuple2<Connection,Statement> conn = db.createConnection();
	    		try {
					conn._2().executeUpdate("UPDATE Bills SET paid='Y' WHERE bill_id="+id);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	    		db.closeConnection(conn);
	        }
	    });

		initBill();

	}
	
	public void initBill(){
		DBConnector db = new DBConnector("jdbc:mysql://localhost:3306/7dobles","sout","sout00");
		Tuple2<Connection,Statement> conn = db.createConnection();
		try {
			ResultSet rset=conn._2().executeQuery("SELECT * FROM Bills WHERE table_id="+id+" AND paid='N'");
			if(rset.next()){ // cheking if there's a bill creted or not
				updateBill();
			}
			else{
				conn._2().executeUpdate("INSERT INTO Bills VALUES(NULL,NULL,'N',CURRENT_DATE,CURRENT_TIME,"+id+",NULL)"); //we create the bill
				updateBill();
			}
			rset.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		db.closeConnection(conn);
	}
	private void updateBill(){
		clear();
		DBConnector db = new DBConnector("jdbc:mysql://localhost:3306/7dobles","sout","sout00");
		Tuple2<Connection,Statement> conn = db.createConnection();
		bill.append("Mesa n�"+id+"\n");
		bill.append("---------\n");
		try {
			ResultSet rset=conn._2().executeQuery("SELECT COUNT(*), ser_name, price FROM Bills JOIN Contains USING(bill_id) JOIN Services USING(service_id) WHERE table_id="+id+" GROUP BY service_id");
			while(rset.next()){
				int quantity = rset.getInt(1);
				double price=rset.getDouble(3)*quantity;
				total+=price;
				String name=rset.getString(2);
				bill.append(String.valueOf(quantity)+" x "+name+" - "+price+" �");
				bill.append("\n");	
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		bill.append("---------\n");
		bill.append("Total="+total+" �");
	}
			
	public void controller(ActionListener ctr) {

	}
	
	public void error(String m) {
		
	}

	public void ok(String m) {
		
	}

	public void addLogEntry(String message) {
		
	}

	public void clear() {
		bill.setText("");
	}
	
	/*
	 * Returns the amount of items to add to the bill.
	 * If the JTF is empty returns 0.
	 */
	public int getQuantity(){
		if(quantityJTF.getText().equals(""))
			return 0;
		else
			return Integer.parseInt(quantityJTF.getText());
	}
	
}
