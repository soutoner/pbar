/*
 * Adrian Gonzalez Leiva, 2014
 * 
 * Siete Dobles Management App (bar).
 */

package gui;

import java.awt.*;
import java.awt.event.*;
import java.sql.*;

import javax.swing.*;

import models.DBConnector;
import dataStructures.tuple.Tuple2;

public class Panel extends JPanel implements View{
	private static final long serialVersionUID = 1L;
	private static final int BUTTONS_CAPACITY=20;
	
	private JButton [] buttons; // array of buttons (tables)
	private boolean [] active;	// table - active or not ?
	private int [] ids; // table ids
	private JPanel up1;
	private JPanel down1;
		
	public Panel(){
			
		setLayout(new BorderLayout()); // Main panel FlowLayout()
		
			JPanel back=new JPanel();
			back.setLayout(new GridLayout(3,1));
			
				JPanel up=new JPanel();
				up.setBorder(BorderFactory.createTitledBorder("Mesas inactivas"));
				
					up1=new JPanel();
					up1.setLayout(new GridLayout(5,4));
					
					up1.add(new JTextArea("Aqui van las mesas inactivas"));
					
				up.add(up1);
				
				JPanel down=new JPanel();
				down.setBorder(BorderFactory.createTitledBorder("Mesas activas"));
				
					down1=new JPanel();
					down1.setLayout(new GridLayout(5,4));
					
					down1.add(new JTextArea("Aqui van las mesas donde hay gente"));
										
				down.add(down1);
				
				JPanel log=new JPanel();
				log.add(new JTextArea(10,50));
						
			back.add(up);
			back.add(down);
			back.add(log);
				
		add(back,BorderLayout.CENTER);
		add(new JLabel("Hola :-)"),BorderLayout.SOUTH); // END main panel
		
		// Initializing table's buttons
		initButtons();
	}
	
	/*
	 * Initialize the tables buttons checking if they're
	 * active or not.
	*/
	private void initButtons(int n){
		buttons=new JButton[n];
		active= new boolean[n];
		ids= new int[n];
		DBConnector db = new DBConnector("jdbc:mysql://localhost:3306/7dobles","root","");
		Tuple2<Connection,Statement> conn = db.createConnection();
		try {			
			ResultSet rset=conn._2().executeQuery("SELECT table_id, table_name FROM Rest_Tables JOIN Bills USING (table_id)");
			int i=0;
			while(rset.next()){
				buttons[i]=new JButton(rset.getString(2));
				active[i]=true;
				ids[i]=rset.getInt(1);
				i++;
			}
			rset=conn._2().executeQuery("SELECT table_id, table_name FROM Rest_Tables WHERE table_id NOT IN (SELECT table_id FROM Rest_Tables JOIN Bills USING (table_id))");
			while(rset.next()){
				buttons[i]=new JButton(rset.getString(2));
				active[i]=false;
				ids[i]=rset.getInt(1);
				i++;
			}
			rset.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		db.closeConnection(conn);
		
		for(int i=0;i<buttons.length && buttons[i]!=null;i++){
			if(active[i]){
				down1.add(buttons[i]);
			}
			else{
				up1.add(buttons[i]);
			}
			final int id=ids[i];
			final int num=i+1;
			// new window when you press
			buttons[i].addActionListener(new ActionListener() {
		        public void actionPerformed(ActionEvent e) {
		    		JFrame window=new JFrame("Cuenta de la mesa "+num);
		    		window.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		    		window.setContentPane((JPanel)new BillPanel(id));
		    		window.pack();
		    		window.setVisible(true);
		        }
		    });
		}
	}
	/*
	 * If no number is passed as argument, the default number of tables is 20.
	 */
	private void initButtons(){
		initButtons(BUTTONS_CAPACITY);
	}

	public void controller(ActionListener ctr) {
	}

	public void error(String m) {
		
	}

	public void ok(String m) {
		
	}

	public void addLogEntry(String message) {
		
	}

	public void clear() {
		
	}
	
}
