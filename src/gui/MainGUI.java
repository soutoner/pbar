/*
 * Adrian Gonzalez Leiva, 2014
 * 
 * Main of the App.
 */

package gui;

import javax.swing.JFrame;
import javax.swing.JPanel;

import models.DBConnector;

public class MainGUI {
	public static void main(String[]args){
		View view=new Panel();
		DBConnector db = new DBConnector("jdbc:mysql://localhost:3306/7dobles","root","");		
		Controller ctr=new Controller(view,db);
		
		view.controller(ctr);
		
		JFrame window=new JFrame("Siete Dobles App");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setContentPane((JPanel)view);
		window.pack();
		window.setVisible(true);
	}
}
