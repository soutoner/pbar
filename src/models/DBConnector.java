/*
 * Adrian Gonzalez Leiva, 2014
 * 
 * Data base management.
 */

package models;

import java.sql.*;
import dataStructures.tuple.*;

public class DBConnector {
	private String connection;	
	private String user;
	private String pass;
	
	public DBConnector(String con, String user, String pass){
		connection=con;
		this.user=user;
		this.pass=pass;
	}
	/*
	 * Creates a connection to a database Tuple(connection,statement)
	 * (*) This method must be used along with closeConnection
	 */
	public Tuple2<Connection,Statement> createConnection(){
		Connection conn=null;
		Statement stmt=null;
		 try{
			 DriverManager.registerDriver(new com.mysql.jdbc.Driver());
	         conn = DriverManager.getConnection(connection,user,pass);
	         System.out.println("Connected to Database :-)");
	         stmt=conn.createStatement();
	        }catch(SQLException x){
	            x.printStackTrace();
	        }
		 return new Tuple2<Connection, Statement>(conn,stmt);
	}
	
	/*
	 * Closes a connection and a statement
	 * (*) This method must be used always when createConnection() is used
	 */
	public void closeConnection(Tuple2<Connection,Statement> conn){
		try {
			conn._2().close(); // we close the stmt
			conn._1().close(); // we close the conn
			System.out.println("Connection closed :-(");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String [] args) throws SQLException{
		DBConnector db = new DBConnector("jdbc:mysql://localhost:3306/7dobles","sout","sout00");
		Tuple2<Connection, Statement> conn = db.createConnection();
		conn._2().executeUpdate("CREATE TABLE hola(id INT, ad VARCHAR(1));");
		db.closeConnection(conn);
	}
}
